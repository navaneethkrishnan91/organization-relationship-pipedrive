<?php

Route::get('organization/{name}/{page?}', 'OrganizationController@show');
Route::post('organization', 'OrganizationController@store');
