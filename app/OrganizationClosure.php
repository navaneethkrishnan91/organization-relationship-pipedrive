<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrganizationClosure extends Model
{
    protected $table = 'org_tree';
    public $timestamps = false;

    /**
     * Organization closure table insert statement
     *
     * @param $ancestor Ancestor of the node
     * @param $descendant Descendant of the node
     */
    public static function store($ancestor, $descendant)
    {
        DB::insert("INSERT INTO org_tree_closure (ancestor, descendant, depth)
                    SELECT ancestor, '{$ancestor}', depth+1 FROM org_tree_closure
                    WHERE descendant = '{$descendant}'
                    UNION ALL SELECT '{$ancestor}', '{$ancestor}', 0");
    }
}
