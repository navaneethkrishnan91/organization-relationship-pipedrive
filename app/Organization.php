<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Organization
 * @package App
 *
 * @property name string Organization name
 * @property parent_id int Parent of Organization
 */
class Organization extends Model
{
    protected $table = 'org_tree';

    public $timestamps = false;
}
