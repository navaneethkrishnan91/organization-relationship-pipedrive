<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Organization;
use App\OrganizationClosure;

class OrganizationController extends Controller
{
    /**
     * Store request data
     *
     * @param Request $request
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $this::insert([json_decode($request->getContent(), true)]);
    }

    /**
     * Recursive function to insert array data in closure tables
     *
     * @param $array
     * @param null $parent Parent id of the organization
     * @throws \Exception
     */
    protected static function insert($array, $parent = null)
    {
        try {
            foreach ($array as $item) {
                $children = array_pull($item, 'daughters');

                $organization = new Organization();
                $organization->name = $item['org_name'];
                $organization->parent_id = $parent;
                $organization->save();

                $descendant = $parent == null ? $organization->id : $parent;
                OrganizationClosure::store($organization->id, $descendant);

                if (!is_null($children)) {
                    self::insert($children, $organization->id);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Show results of queried item with immediate parents, daughters and siblings
     *
     * @param $name
     * @param int $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($name, $page = 1)
    {
        $request = new Request([
            'name' => $name,
            'page' => $page
        ]);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'page' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $response = \DB::select('call findByName(?,?,?)', array($name, $page, 100));

        return response()->json($response);
    }
}
