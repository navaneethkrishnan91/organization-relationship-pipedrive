>Overview:

Given the hierarchical nature of the data, I have implemented the task using closure tables.
Following is the brief overview of the implementation.

1. Organization Tree table to store the organization names and corresponding parent ids.
2. Organization Closure table to store ancestor, descendant and depth columns which stores the tree structure.
3. Procedure with pagination support to fetch all immediate siblings, parents and daughters of the queried node.
4. Recursive method to insert input data using raw insert query for closure table insertion.

Source: https://www.percona.com/webinars/2011-02-28-models-for-hierarchical-data-in-sql-and-php

>Requirements:


1. php version >= 5.4 
2. Something to create a post request like "Postman".


>Steps:
```
git clone git@bitbucket.org:navaneethkrishnan91/organization-relationship-pipedrive.git
```
```
cd <path_to_project>/public
```
 
```
php -S localhost:8080
```
or 
```
php artisan serve
```
Set database credentials in the .env file.

Run the migration scripts:
```
php artisan migrate
```


>Routes: 

Search for an organization:
-  GET: http://localhost:8080/api/organization/{name}/{?page}

Insert data:
- POST: http://localhost:8080/api/organization