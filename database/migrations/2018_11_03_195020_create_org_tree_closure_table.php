<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateOrgTreeClosureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('org_tree_closure', function (Blueprint $table) {
            $table->integer('ancestor')->unsigned();
            $table->integer('descendant')->unsigned();
            $table->smallInteger('depth')->unsigned();

            $table->primary(['ancestor', 'descendant']);
            $table->index(['ancestor', 'descendant', 'depth']);
            $table->index(['descendant', 'depth']);

            $table->foreign('ancestor')->references('id')->on('org_tree');
            $table->foreign('descendant')->references('id')->on('org_tree');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('org_tree_closure');
    }
}
