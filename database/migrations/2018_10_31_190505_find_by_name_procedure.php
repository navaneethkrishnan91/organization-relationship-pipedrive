<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class FindByNameProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
            create or replace procedure findByName(org_name varchar(25), page int(11), per_page int(11))
              BEGIN
                declare off int(11) default 0;
                set off = (page - 1) * per_page;
            
                select distinct m.name, 'sibling' as relation
                from org_tree_closure a
                       join org_tree_closure s on s.ancestor = a.ancestor
                       join org_tree m on m.id = s.descendant
                where a.descendant in (select id from org_tree where name = org_name)
                  and s.depth = a.depth
                  and m.name <> org_name
                union
                select name, 'parent' as relation
                from org_tree_closure
                       join org_tree
                         on (org_tree_closure.ancestor = org_tree.id)
                where org_tree_closure.descendant in
                      (select id from org_tree where name = org_name)
                  and depth = 1
                union
                select name, 'children' as relation
                from org_tree_closure
                       join org_tree
                         on (org_tree_closure.descendant = org_tree.id)
                where org_tree_closure.ancestor in
                      (select id from org_tree where name = org_name)
                  and depth = 1
                order by name
                limit per_page offset off;
              END;
        ";
        DB::unprepared("DROP procedure IF EXISTS findByName");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE findByName");
    }
}
